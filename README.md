# Rusty AUR Helper (RAH)
RAH is an AUR helper, used for installing packages from the Arch User Repository (or AUR), written in Rust. Since RAH uses [Pacman](https://wiki.archlinux.org/title/pacman) and [makepkg](https://wiki.archlinux.org/title/makepkg), it's expected to only work on Arch GNU/Linux and other Arch-based distributions.

**Note:** This program has only been tested on vanilla Arch GNU/Linux, please report any bugs you may encounter on other Arch-based distributions.

## Notice
This project is licensed under the MIT License. See [LICENSE](LICENSE) for copyright and license details.

## Installation
This project is available as an [AUR package](https://aur.archlinux.org/packages/rah), so you can install it with another AUR helper or build with [makepkg](https://wiki.archlinux.org/title/makepkg).

If you are installing without the AUR package, you will need to install [Git](https://archlinux.org/packages/extra/x86_64/git/) as a dependency.

### Compilation
To compile locally, run the following command:
```shell
cargo build -r
```
The executable can be found in `./target/release/rah`.

## Usage
*<> = Required, [] = Optional*

| Command - Short       | Command - Long                   | Description                  |
| --------------------- | -------------------------------- | ---------------------------- |
| `rah -I <package(s)>` | `rah --install <package(s)>`     | Install package(s) from AUR. |
| `rah -U [package(s)]` | `rah --update [package(s)]`      | Update package(s).           |
| `rah -R <package(s)>` | `rah --remove <package(s)>`      | Uninstall package(s).        |
| `rah -Q [package(s)]` | `rah --query [package(s)]`       | Query package(s).            |
| `rah -C [package(s)]` | `rah --clear-cache [package(s)]` | Clear package(s) from cache. |