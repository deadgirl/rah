/**
* This file contains the "Package" struct, used for updating and/or checking
* versions of packages.
*
* @author Madison Lynch
*/
use std::{
    error::Error,
    fs,
    path::PathBuf,
};

pub struct Package {
    pub name: String,
    pub version: String
}

impl Package {
    pub fn new(name: String) -> Self {
        Self {
            name,
            version: String::new()
        }
    }

    /**
    * Populates "current_ver" struct field with the currently installed
    * version.
    *
    * @param  path  mutable PathBuf struct used to find/verify packages in cache
    * @return       returns "Ok(())" if successful, "Err({message})" otherwise
    */
    pub fn get_version(&mut self, path: &PathBuf) -> Result<(), Box<dyn Error>> {
        let pkgbuild_contents: String = fs::read_to_string(
            format!(
                "{}/{}/PKGBUILD",
                path.display(),
                self.name
            )
        ).expect("Failed to open PKGBUILD file (invalid package)");

        for line in pkgbuild_contents.lines() {
            if line.contains("pkgver") {
                self.version = line.to_string();
                break;
            }
        }
        if self.version.is_empty() {
            self.version = "unknown".to_string();
        }

        Ok(())
    }
}