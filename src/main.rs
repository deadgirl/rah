/**
* Name: RAH - Rusty AUR Helper
* Written by Madison Lynch <madi@mxdi.xyz>
* Source: https://gitlab.com/deadgirl/rah
* License: MIT (See LICENSE file for copyright and license details)
*
* Overview:
* RAH is a simple AUR helper; a program which simplifies the process of
* installing packages from the Arch User Repository. See help command,
* README, or man page for usage information.
*/
use std::{
    env,
    error::Error,
    fs,
    io::{
        stdin,
        stdout,
        Write
    },
    path::PathBuf,
    process::{
        Command,
        Stdio
    }
};

use libc::getuid;
use which::which;

/*
* Contains Package struct.
*
* @see package.rs
*/
mod package;
use package::Package;

const AUR_URL: &str = "https://aur.archlinux.org";

/**
* Prints help menu with usage info to stdout.
*
* @return will always return "Ok(())"
*/
fn print_help() -> Result<(), Box<dyn Error>> {
    println!("RAH (Rusty AUR Helper) is a simple AUR helper written in Rust.");
    println!();
    println!("Usage: rah <operation> [...]");
    println!("Operations:");
    println!();
    
    println!("\trah {{-I, --install}} <package(s)>:");
        println!("\t\tInstall package(s)\n");
    println!("\trah {{-U, --update}} [package(s)]:");
        println!("\t\tUpdate packages (no args checks for all cached packages)\n");
    println!("\trah {{-R, --remove}} <package(s)>:");
        println!("\t\tUninstall package(s)\n");
    println!("\trah {{-Q, --query}} [package(s)]:");
        println!("\t\tSee what package(s) are tracked by RAH\n");
    println!("\trah {{-C, --clear-cache}} [package(s)]:");
        println!("\t\tClear package cache (no args clears all; not advised unless you know what you're doing)\n");
    println!("\trah {{-h, --help}}:");
        println!("\t\tPrint help menu");

    Ok(())
}

/**
* Prints info (name, version) for packages tracked by RAH.
*
* Will only print specified packages if included, otherwise all packages found
* in cache will be printed.
*
* @param  args  command line arguments
* @param  path  mutable Path struct used to find/verify packages in cache
* @return       returns "Ok(())" if successful, "Err({message})" otherwise
*/
fn query_packages(args: Vec<String>, mut path: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut targets: Vec<Package> = Vec::new();

    if args.len() < 3 {
        for target in path.read_dir()? {
            // Not expected to fail
            let target = target?;

            if target.file_type()?.is_dir() {
                targets.push(
                    Package::new(
                        target
                            .file_name()
                            .into_string()
                            .unwrap()
                    )
                )
            }
        }

        if targets.is_empty() {
            return Ok(())
        }

        println!("Packages Tracked by RAH:");
        println!("------------------------\n");
    } else {
        for target in args.iter().skip(2) {
            path.push(target);
            if path.exists() && path.is_dir() {
                targets.push(
                    Package::new(target.clone())
                );
            } else {
                println!("{target} is not installed");
            }
            path.pop();
        }
    }

    for mut package in targets {
        package.get_version(&path)?;
        
        println!("{} ({})", package.name, package.version);
    }

    Ok(())
}

/**
* Clears packages from user's cache directory.
*
* If no packages are specified, all packages will be cleared.
* Note: Clearing cache != uninstalling package.
*
* @param  args  command line arguments
* @param  path  mutable Path struct used to find/remove packages in cache
* @return       returns "Ok(())" if successful, "Err({message})" otherwise
*/
fn clear_cache(args: Vec<String>, mut path: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut targets: Vec<String> = Vec::new();

    if args.len() < 3 {        
        for target in path.read_dir()? {
            // Not expected to fail
            let target = target?;

            if target.file_type()?.is_dir() {
                targets.push(
                    target
                        .file_name()
                        .into_string()
                        .unwrap()
                );
            }
        }

        if targets.is_empty() {
            return Ok(());
        }
    } else {
        for target in args.iter().skip(2) {
            path.push(target);

            if !path.exists() || !path.is_dir() {
                return Err(
                    format!("{target} not found in cache").into()
                );
            }

            targets.push(target.clone());
            path.pop();
        }
    }

    for target in targets {
        fs::remove_dir_all(
            format!(
                "{}/{}",
                path.display(),
                target
            )
        )?;
    }

    Ok(())
}

/**
* Removes specified packages using Pacman (Arch Linux package manager). Only
* works with sudo, doas, or su.
*
* @param  targets  packages to uninstall
* @return          returns "Ok(())" if successful, "Err({message})" otherwise
*/
fn remove_packages(targets: Vec<String>) -> Result<(), Box<dyn Error>> {
    if targets.len() < 3 {
        return Err("No targets supplied".into());
    }

    let auth: String = (match which("sudo") {
        Ok(path) => path,
        Err(_) => match which("doas") {
            Ok(path) => path,
            Err(_) => match which("su") {
                Ok(path) => path,
                Err(_) => return Err(
                    "Could not find sudo, doas, or su on system".into()
                )
            }
        }
    }).display().to_string();

    let pacman_args: [String; 3] = [
        which("pacman")?
            .display()
            .to_string(),
        "-Rns"
            .to_string(),
        targets[2..]
            .join(" ")
    ];

    if auth.contains("sudo") || auth.contains("doas") {
        Command::new(auth)
            .args(&pacman_args)
            .status()
            .expect("Failed to run pacman");
    } else {
        println!("Authorizing with su (root)");
        Command::new(auth)
            .args(["root", "-c"])
            .args(&pacman_args)
            .status()
            .expect("Failed to run pacman");
    }

    Ok(())
}

/**
* Checks package versions and updates if appropriate. Will check and update all
* packages in cache if no packages are specified.
*
* @param  args  command line arguments
* @param  path  mutable Path struct used to find/verify packages in cache
* @return       returns "Ok(())" if successful, "Err({message})" otherwise
*/
fn update_packages(args: Vec<String>, mut path: PathBuf) -> Result<(), Box<dyn Error>> {
    let mut targets: Vec<Package> = Vec::new();

    if args.len() < 3 {
        for target in path.read_dir()? {
            // Not expected to fail
            let target = target?;

            if target.file_type()?.is_dir() {
                targets.push(
                    Package::new(
                        target
                            .file_name()
                            .into_string()
                            .unwrap()
                    )
                );
            }
        }
    } else {
        for target in args.iter().skip(2) {
            path.push(target);

            if path.exists() && path.is_dir() {
                targets.push(
                    Package::new(target.clone())
                );
            } else {
                return Err(
                    format!("Specified target ({target}) not found in cache").into()
                );
            }

            path.pop();
        }
    }

    for target in targets {
        println!("Checking {}", target.name);
        path.push(&target.name);

        let current_commit_hash: String = String::from_utf8((
            Command::new(which("git")?)
                .args([
                    "rev-parse",
                    "--short",
                    "HEAD"
                ])
                .current_dir(&path)
                .output()
                .expect("Failed to get current commit hash")
        ).stdout)?;

        let pull_output: String = String::from_utf8((
            Command::new(which("git")?)
                        .arg("pull")
                        .current_dir(&path)
                        .output()
                        .expect("Git pull failed")
        ).stdout)?;

        if pull_output.trim_end() != "Already up to date." {
            let mut input: String = String::new();
            print!("Upgrade {}? [Y/n] ", target.name);
            let _ = stdout().flush();
            stdin()
                .read_line(&mut input)
                .expect("Failed to read stdin");

            match input.to_lowercase().trim_end() {
                "y" | "yes" | "" =>
                    Command::new(which("makepkg")?)
                        .arg("-sri")
                        .current_dir(&path)
                        .status()
                        .expect("Failed to run makepkg"),
                _ => Command::new(which("git")?)
                        .args([
                            "reset",
                            "--hard",
                            current_commit_hash.trim()
                        ])
                        .current_dir(&path)
                        .stdout(Stdio::null())
                        .status()
                        .expect("Failed to revert cache to previous version")
            };
        }
        path.pop();
    }

    Ok(())
}

/**
* Uses Git to clone the package from the AUR and installs with makepkg.
*
* @param  args  command line arguments
* @param  path  mutable Path struct used to place  packages in cache
* @return       returns "Ok(())" if successful, "Err({message})" otherwise
*/
fn install_packages(args: Vec<String>, mut path: PathBuf) -> Result<(), Box<dyn Error>> {
    if args.len() < 3 {
        return Err("No targets supplied".into());
    }

    for target in args.iter().skip(2) {
        let url: String = format!("{}/{}", AUR_URL, target);
        path.push(target);

        if !path.exists() {
            match git2::Repository::clone(&url, &path) {
                Ok(_) => (),
                Err(_) => return Err(
                    format!("Failed to download target: {target}").into()
                )
            };

            path.push("PKGBUILD");
            if !path.exists() {
                eprintln!("Target ({target}) not found");

                path.pop();
                fs::remove_dir_all(&path)?;
                path.pop();
                continue;
            }
            path.pop();
        }

        Command::new(which("makepkg")?)
            .arg("-sri")
            .current_dir(&path)
            .status()
            .expect("Failed to run makepkg");

        path.pop();
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    // Check to see if user is running as root
    unsafe {
        if getuid() == 0 {
            return Err("Running RAH as root is not allowed".into());
        }
    }

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        return Err("No arguments passed. Use '-h' flag for help".into());
    }

    let cache_path: PathBuf = PathBuf::from(
        format!(
            "{}/.cache/rah",
            env::var("HOME")
                .expect("HOME environment variable not set")
        )
    );
    fs::create_dir_all(&cache_path)
        .expect("Failed to initialize cache directory");

    match args[1].as_str() {
        "-I" | "--install"     => install_packages(args, cache_path),
        "-U" | "--update"      => update_packages(args, cache_path),
        "-R" | "--remove"      => remove_packages(args),
        "-Q" | "--query"       => query_packages(args, cache_path),
        "-C" | "--clear-cache" => clear_cache(args, cache_path),
        "-h" | "--help"        => print_help(),
        _ => Err("Invalid usage. Pass '-h' flag for help".into())
    }
}